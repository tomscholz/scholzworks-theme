<?php get_header(); ?>

		<?php query_posts( 'posts_per_page=10'); while(have_posts()) : the_post(); ?>

			<div>
				<h2 class="blue-text"><?php the_title(); ?></h2>
				<p><?php the_author_posts_link(); ?></p>
				<p><?php the_excerpt(); ?></p>
			</div>

		<?php endwhile; wp_reset_query(); ?>

<?php get_footer(); ?>