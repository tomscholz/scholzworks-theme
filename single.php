<?php get_header(); ?>
<div class="container">
	<div id="main">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h2 class="blue-grey-text"><?php the_title(); ?></h2>
			<div id="meta">
				<p><?php the_date( 'd.m.Y'); ?> <?php the_time( 'H:i'); ?> | <?php the_author_posts_link(); ?></p>
			</div>
			<div class="entry">
				<?php the_content(); ?>
			</div>
		<div><?php next_posts_link( 'Older posts' ); ?></div>
<div><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php endwhile; endif; ?>
	</div>
<?php comments_template(); ?>
</div>
<?php get_footer(); ?>