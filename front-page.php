<?php get_header(); ?>
	<header>
		<div class="slider" style="height: auto !important;">
			<ul class="slides">
				<li>
					<img src="/wp-content/uploads/2016/03/datacenter-5.jpg">
					<div class="caption left-align">
						<h2><span>Exzellenter Service</span></h2>
						<h3 class="light white-text text-lighten-1"><span>Bei <logo>SCHOLZ<span style="color: #ff5400;">works</span></logo> können Sie sich auf exzellenten Service verlassen.  Ich lasse Sie nicht im Stich!</span></h3>
						<h5 class="light grey-text text-lighten-3"><a href="/hosting/" class="waves-effect waves-light btn orange">Jetzt Informieren</a></h5>
					</div>
				</li>
				<li>
					<img src="/wp-content/uploads/2016/03/google.png">
					<div class="caption left-align">
						<h2><span>Google ready!</span></h2>
						<h3 class="light white-text text-lighten-1"><span>Seit 2012 werden von <logo>SCHOLZ<span style="color: #ff5400;">works</span></logo> alle neuen Webseiten Responsive erstellt!</span></h3>
						<h5 class="light grey-text text-lighten-3"><a href="/webdesign/" class="waves-effect waves-light btn orange">Jetzt Informieren</a></h5>
					</div>
				</li>
				<li>
					<img src="/wp-content/uploads/2016/03/datacenter-8.jpg">
					<div class="caption left-align">
						<h2><span>Sicherheit? Na klar!</span></h2>
						<h3 class="light white-text text-lighten-1"><span>Seit 2016 erhalten alle von <logo>SCHOLZ<span style="color: #ff5400;">works</span></logo> erstellten Seiten kostenlose SSL Zertifikate!</span></h3>
						<h5 class="light grey-text text-lighten-3"><a href="/domains/" class="waves-effect waves-light btn orange">Jetzt Informieren</a></h5>
					</div>
				</li>
			</ul>
		</div>
	</header>
		<div class="container no">
			<div class="section">
			<!--	Icon Section	-->
				<div class="row card-panel">
					<div class="col s12 m4">	
						<div class="row center card-panel icon-panel hoverable">
							<div class="icon-block">
								<h2 class="center light-blue-text"><i class="<?php global $wp_query; $postid = $wp_query->post->ID; echo get_post_meta($postid, 'home_icon_1', true); ?>"></i></h2>
								<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Start Widget 1')):else: ?>
								<h4 class="center">Textblock</h5>
								<p class="light">I am an textblock. Click in dashboard on <br> design->widgets to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo</p>
								<?php endif;?>
							</div>
						</div>
					</div>	
					<div class="col s12 m4">	
						<div class="row center card-panel icon-panel hoverable">
							<div class="icon-block">
								<h2 class="center light-blue-text"><i class="<?php global $wp_query; $postid = $wp_query->post->ID; echo get_post_meta($postid, 'home_icon_1', true); ?>"></i></h2>
								<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Start Widget 2')):else: ?>
								<h4 class="center">Textblock</h5>
								<p class="light">I am an textblock. Click in dashboard on <br> design->widgets to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo</p>
								<?php endif;?>
							</div>
						</div>
					</div>	
					<div class="col s12 m4">	
						<div class="row center card-panel icon-panel hoverable">
							<div class="icon-block">
								<h2 class="center light-blue-text"><i class="<?php global $wp_query; $postid = $wp_query->post->ID; echo get_post_meta($postid, 'home_icon_1', true); ?>"></i></h2>
								<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Start Widget 3')):else: ?>
								<h4 class="center">Textblock</h5>
								<p class="light">I am an textblock. Click in dashboard on <br> design->widgets to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo</p>
								<?php endif;?>
							</div>
						</div>
					</div>	
				</div>
				</div>
				</div>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="entry">
					<?php the_content(); ?>
				</div>
				<?php endwhile; endif; ?>
			
		
			
<?php get_footer(); ?>