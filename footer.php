	<footer class="page-footer">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer About')):else: ?>
						<h4 class="white-text">Lorem Ipsum</h4>
						<p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
					<?php endif; ?>
				</div>
				<div class="col l3 s12 menua">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 1')): ?>
					<h4 class="white-text">Mehr zum Thema...</h4>
					<ul>
						<li><a class="white-text" href="#!">Link 1</a></li>
						<li><a class="white-text" href="#!">Link 2</a></li>
						<li><a class="white-text" href="#!">Link 3</a></li>
						<li><a class="white-text" href="#!">Link 4</a></li>
					</ul>
					<?php endif; ?>
				</div>
				<div class="col l3 s12">
					<ul>
						<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widget 2')): ?>
					<h4 class="white-text">Social</h4>
					<ul>
						<li><a class="white-text" href="#!"><i class="small fa fa-twitter-square white-text"></i> Twitter</a></li>
						<li><a class="white-text" href="https://www.facebook.com/Scholzworks"><i class="small fa fa-facebook-square white-text"></i> Facebook</a></li>
						<li><a class="white-text" href="https://github.com/scholzworks"><i class="small fa fa-bitbucket-square white-text"></i> BitBucket</a></li>
						<li><a class="white-text" href="https://www.linkedin.com">´<i class="small fa fa-xing-square white-text"></i> Xing</a></li>
					</ul>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				<?php echo date('Y'); ?> ScholzWorks.com
				<a style="padding-left:10px;" class="grey-text text-lighten-4 right" href="/datenschutz/">Datenschutz</a>
				<a style="padding-right:10px;" class="grey-text text-lighten-4 right" href="/impressum/">Impressum</a>
			</div>
		</div>
	</footer>
	
	<?php wp_footer();?>

	<!--  Scripts-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/materialize.min.js"></script>
	<script src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/init.js"></script>
	<script src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
	<script src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/js/main.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://analytics.scholzworks.com/plugins/ClickHeat/libs/js/clickheat.js"></script><script type="text/javascript"><!--
		clickHeatSite = 1;clickHeatGroup = encodeURIComponent(window.location.pathname+window.location.search);clickHeatServer = 'https://analytics.scholzworks.com/plugins/ClickHeat/libs/click.php';initClickHeat(); //-->
	</script>
</body>

</html>