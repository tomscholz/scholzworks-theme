<?php
// Register Custom Post Type
function scholzworks_referenz_post_type() {

	$labels = array(
		'name'                  => _x( 'Referenzen', 'Post Type General Name', 'scholzworks' ),
		'singular_name'         => _x( 'Referenz', 'Post Type Singular Name', 'scholzworks' ),
		'menu_name'             => __( 'Referenzen', 'scholzworks' ),
		'name_admin_bar'        => __( 'Referenzen', 'scholzworks' ),
		'archives'              => __( 'Referenzen Archives', 'scholzworks' ),
		'parent_item_colon'     => __( 'Parent Item:', 'scholzworks' ),
		'all_items'             => __( 'Alle Referenzen', 'scholzworks' ),
		'add_new_item'          => __( 'Neue Referenz hinzuf�gen', 'scholzworks' ),
		'add_new'               => __( 'Neue Referenz hinzuf�gen', 'scholzworks' ),
		'new_item'              => __( 'Neue Referenz', 'scholzworks' ),
		'edit_item'             => __( 'Referenz bearbeiten', 'scholzworks' ),
		'update_item'           => __( 'Update Referenz', 'scholzworks' ),
		'view_item'             => __( 'Referenz ansehen', 'scholzworks' ),
		'search_items'          => __( 'Referenz suchen', 'scholzworks' ),
		'not_found'             => __( 'Nicht gefunden', 'scholzworks' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'scholzworks' ),
		'featured_image'        => __( 'Beitragsbild', 'scholzworks' ),
		'set_featured_image'    => __( 'Beitragsbild festlegen', 'scholzworks' ),
		'remove_featured_image' => __( 'Beitragsbild entfernen', 'scholzworks' ),
		'use_featured_image'    => __( 'Als Beitragsbild verwenden', 'scholzworks' ),
		'insert_into_item'      => __( 'In Referenz einf�gen', 'scholzworks' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Referenz', 'scholzworks' ),
		'items_list'            => __( 'Referenzen liste', 'scholzworks' ),
		'items_list_navigation' => __( 'Referenzen liste navigation', 'scholzworks' ),
		'filter_items_list'     => __( 'Referenzen liste filtern', 'scholzworks' ),
	);
	$rewrite = array(
		'slug'                  => 'referenz',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Referenz', 'scholzworks' ),
		'description'           => __( 'Referenzen beschreibung', 'scholzworks' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'post-formats', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => ' dashicons-list-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'referenz_type', $args );

}
add_action( 'init', 'scholzworks_referenz_post_type', 0 );