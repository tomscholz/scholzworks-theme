<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package scholzworks
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="card hoverable">
		<div class="card-image waves-effect waves-block waves-light">

			<?php
				$default_attr = array(
					'class' => "activator",
				);
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					the_post_thumbnail(array(1024, 520), $default_attr );
				}
			?>
		</div>
		<div class="card-content">
			<span class="card-title activator grey-text text-darken-4 entry-title">
				<?php the_title(); ?>
				<i class="material-icons right">more_vert</i>
			</span>
			<div class="entry-meta">
				<?php fz_posted_on(); ?>
			</div><!-- .entry-meta -->
		</div>
		<div class="card-action right-align">
			<p><a href="<?php echo get_permalink(); ?>">Mehr dazu...</a></p>
		</div>
		<div class="card-reveal">
			<span class="card-title activator grey-text text-darken-4 entry-title ">
				<?php the_title(); ?>
				<i class="material-icons right">close</i>
			</span>
			<div class="entry-excerpt">
			<?php
				the_excerpt( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'scholzworks' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
			</div><!-- .entry-excerpt -->
		</div>

	</div>

</article><!-- #post-## -->
