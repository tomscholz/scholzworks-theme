<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>
		<nav class="white" style="margin-bottom: 20px;">
			<div class="container no">
				<div class="row">
					<div class="col l9">
						<h2><?php single_post_title(); ?></h2>
					</div>
					<div class="col l3 hide-on-med-and-down">
						<a href="/" class="breadcrumb">Home</a>
						<a href="<?php global $post; $post_slug=$post->post_name;?>" class="breadcrumb"><?php single_post_title(); ?></a>
					</div>
				</div>
			</div>
		</nav>
	</header>	  
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

