<?php
// use widgets_init action hook to execute custom function
add_action( 'widgets_init', 'scholzworks_social_widget' );

 //register our widget
function scholzworks_social_widget() {
register_widget( 'scholzworks_social_widget' );
}

//boj_widget_my_info class
class scholzworks_social_widget extends WP_Widget {

//process the new widget
function scholzworks_social_widget() {
$widget_ops = array(
'classname' => 'scholzworks_social_widget_class',
'description' => 'SCHOLZworks Social Footer Widget'
);
$this->WP_Widget( 'scholzworks_social_widget', 'SCHOLZworks Social Widget', $widget_ops );
}

//build the widget settings form
function form($instance) {
$defaults = array( 'title' => 'SCHOLZworks Social Widget', 'socialcon' => 'language', 'link' => 'https://' );
$instance = wp_parse_args( (array) $instance, $defaults );
$socialcon = $instance['socialcon'];
$title = $instance['title'];
$link = $instance['link'];
?>
<p>Social:</p>

<p>Icon: <input class="widefat" name="<?php echo $this->get_field_name( 'socialcon' ); ?>"  type="text" value="<?php echo esc_attr( $socialcon ); ?>" /></p>
<p>Title: <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>"  type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
<p>Link: <input class="widefat" name="<?php echo $this->get_field_name( 'link' ); ?>"  type="text" value="<?php echo esc_attr( $link ); ?>" /></p>

<?php
}

//save the widget settings
function update($new_instance, $old_instance) {
$instance = $old_instance;
$instance['socialcon'] = ( $new_instance['socialcon'] );
$instance['title'] = strip_tags( $new_instance['title'] );
$instance['link'] = strip_tags( $new_instance['link'] );

return $instance;
}

//display the widget
function widget($args, $instance) {
extract($args);

echo $before_widget;
$socialcon = empty( $instance['socialcon'] ) ? 'language' : $instance['socialcon'];
$title = apply_filters( 'widget_title', $instance['title'] );
$link = empty( $instance['link'] ) ? '&nbsp;' : $instance['link'];

echo '
<li><a class="white-text" href="' . $link . '"><i class="small fa fa-'. $socialcon .' white-text"> </i> ' . $title . '</a></li>
';
echo $after_widget;
}
}
?>