<?php
// use widgets_init action hook to execute custom function
add_action( 'widgets_init', 'scholzworks_icon_widget' );

 //register our widget
function scholzworks_icon_widget() {
register_widget( 'scholzworks_icon_widget' );
}

//boj_widget_my_info class
class scholzworks_icon_widget extends WP_Widget {

//process the new widget
function scholzworks_icon_widget() {
$widget_ops = array(
'classname' => 'mt_icon_text_widget_class',
'description' => 'Sidebar Box Widget.'
);
$this->WP_Widget( 'scholzworks_icon_widget', 'SCHOLZworks Icon Widget', $widget_ops );
}

//build the widget settings form
function form($instance) {
$defaults = array( 'title' => 'SCHOLZworks Icon Widget', 'description' => '' , 'iconsize' => 'medium' , 'icontype' => 'language' );
$instance = wp_parse_args( (array) $instance, $defaults );
$icontype = $instance['icontype'];
$iconsize = $instance['iconsize'];
$title = $instance['title'];
$description = $instance['description'];
// Todo saubere Lösung finden (Zu viel Code) Excel tabelle parsen
?>
<p>Icon:

<input value="<?php echo esc_attr( $icontype ); ?>" type="text" id="<?php echo $this->get_field_id( 'icontype' ); ?>" name="<?php echo $this->get_field_name( 'icontype' ); ?>"" class="widefat"  style="width:100%;">
<p>Icon size:
<select id="<?php echo $this->get_field_id( 'iconsize' ); ?>" name="<?php echo $this->get_field_name( 'iconsize' ); ?>" class="widefat" style="width:100%;">
<option <?php if ( 'large' == $instance['iconsize'] ){ echo 'selected="selected"';} ?> value="large">Large</option>
<option <?php if ( 'medium' == $instance['iconsize'] ){ echo 'selected="selected"';}  ?> value="medium">Medium</option>
<option <?php if ( 'small' == $instance['iconsize'] ){ echo 'selected="selected"';}  ?> value="small">Small</option>
</select>
<p>Title: <input class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>"  type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
<p>Description: <textarea class="widefat" name="<?php echo $this->get_field_name( 'description' ); ?>" / ><?php echo esc_attr( $description ); ?></textarea></p>

</p>
<?php
}

//save the widget settings
function update($new_instance, $old_instance) {
$instance = $old_instance;
$instance['icontype'] = ( $new_instance['icontype'] );
$instance['iconsize'] = ( $new_instance['iconsize'] );
$instance['title'] = strip_tags( $new_instance['title'] );
$instance['description'] = strip_tags( $new_instance['description'] );

return $instance;
}

//display the widget
function widget($args, $instance) {
extract($args);

echo $before_widget;
$iconsize = empty( $instance['iconsize'] ) ? '' : $instance['iconsize'];
$icontype = empty( $instance['icontype'] ) ? 'language' : $instance['icontype'];
$title = apply_filters( 'widget_title', $instance['title'] );
$description = empty( $instance['description'] ) ? '&nbsp;' : $instance['description'];

echo '
<h2><i class="'. $iconsize .' material-icons">'. $icontype .'</i></h2>
<h4>' . $title . '</h5>
<p>' . $description . '</p>
';
echo $after_widget;
}
}
?>