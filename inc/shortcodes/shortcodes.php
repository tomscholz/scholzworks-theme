<?php
// Add Shortcode
function modal_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'id' => '',
			'title' => '',
			'button' => '',
		), $atts )
	);
	
	// Code
	return '<div id="' . $id . '" class="modal modal-fixed-footer"><div class="modal-content"><h3>' . $title . '</h3><p>' . $content . '</p></div><div class="modal-footer"><a href="#!" class=" modal-action modal-close waves-effect waves-white blue white-text btn-flat">' . $button . '</a></div></div>';
}
add_shortcode( 'modal', 'modal_shortcode' );
	
// Add Shortcode
function parallax_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'src' => '',
		), $atts )
	);

	// Code
return '<div class="parallax-container"><div class="parallax"><img src="' . $src . '"></div></div>';
}
add_shortcode( 'parallax', 'parallax_shortcode' );
?>