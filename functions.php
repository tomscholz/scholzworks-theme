<?php 
/*
 * functions.php
 * 
 */
require('inc/shortcodes/shortcodes.php');
require('inc/widgets/scholzworks_social_widget.php');
require('inc/widgets/scholzworks_icon_widget.php');
// require('inc/scholzworks_referenzen_post_type.php');

function scholzworks_nav_menu() {
	register_nav_menu( 'primary', 'Primary Menu' );
	register_nav_menu( 'footer', 'Footer Menu' );
}  
add_action( 'after_setup_theme', 'scholzworks_nav_menu' );
add_action( 'widgets_init', 'scholzworks_widgets_init' );

register_sidebar(array(
	'name'				=> 'Footer About',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h4 class="white-text">',
	'after_title'		=> '</h4>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

register_sidebar(array(
	'name'				=> 'Footer Widget 1',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h4 class="white-text">',
	'after_title'		=> '</h4>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

register_sidebar(array(
	'name'				=> 'Footer Widget 2',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h4 class="white-text">',
	'after_title'		=> '</h4>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

register_sidebar(array(
	'name'				=> 'Start Widget 1',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h5 class="center">',
	'after_title'		=> '</h5>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

register_sidebar(array(
	'name'				=> 'Start Widget 2',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h5 class="center">',
	'after_title'		=> '</h5>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

register_sidebar(array(
	'name'				=> 'Start Widget 3',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '<h5 class="center">',
	'after_title'		=> '</h5>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));

// Register Sidebar

register_sidebar(array(
	'name'				=> 'Sidebar',
	'before_widget'		=> '',
	'after_widget'		=> '',
	'before_title'		=> '',
	'after_title'		=> '</h5>',
	'description'   => __( 'Put in here a text widget', 'scholzworks' ),
));
?>