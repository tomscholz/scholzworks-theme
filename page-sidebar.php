<?php
/*
Template Name: Page with sidebar
*/
?>
<?php get_header(); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="entry">
					<?php the_content(); ?>
				</div>
			<?php endwhile; endif; ?>
	<div class="col m4 s12">
		<hr class="hide-on-med-and-up">
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>