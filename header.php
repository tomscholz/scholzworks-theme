<!DOCTYPE html>
<html <?php language_attributes(); ?>>

	<head>
		<title><?php wp_title(''); ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<!-- Android 5 Chrome Color -->
		<meta name="theme-color" content="#212121">
		<!-- CSS  -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="//cdn.materialdesignicons.com/1.5.54/css/materialdesignicons.min.css">
		<link href ="<?php bloginfo('stylesheet_url'); ?>" type="text/css" rel ="stylesheet" media="screen,projection"/>
	</head>
	<div id="loader-wrapper">
		<div id="loader"></div>
		<div class="loader-section section-left"></div>
		<div class="loader-section section-right"></div>
	</div>
	<!--wp_head start -->
	<?php wp_head();?>
	<!--wp_head end -->
	<body <?php body_class(); ?>>
	<header>
		<!--  Navigation -->
		<div class="navbar-fixed">
			<nav role="navigation">
				<div class="nav-wrapper container">
					<a id="logo-container" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo active">
						<span class="white-text">SCHOLZ</span><span style="color: #ff5400;">works</span>
					</a>
      <?php
        if(has_nav_menu('primary')) :
          wp_nav_menu(
            array(
              'theme_location' => 'primary',
              'menu_class' => 'right hide-on-med-and-down'
            )
          );
        endif;
        if(has_nav_menu('primary')) :
          wp_nav_menu(
            array(
              'theme_location' => 'primary',
              'menu_id' => 'nav-mobile',
              'menu_class' => 'side-nav center',
            )
          );
        endif;
      ?>
					<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
				</div>
			</nav>
		</div>
	</header>