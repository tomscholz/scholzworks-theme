<?php
/*
Template Name: 100% Width
*/
?>


<?php get_header(); ?>
			<nav class="white" style="margin-bottom: 20px;">
			<div class="container">
				<div class="row">
					<div class="col l9">
						<h2><?php single_post_title(); ?></h2>
					</div>
					<div class="col l3 hide-on-med-and-down">
						<a href="/" class="breadcrumb">Home</a>
						<a href="/hosting/" class="breadcrumb">Hosting</a>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
	<?php endwhile; endif; ?>
<?php get_footer(); ?>